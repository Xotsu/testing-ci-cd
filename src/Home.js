import React, { useState } from 'react';
const axios = require("axios");

function Nav() {
    const [name, inputName] = useState()

    return (
        < div className="Main App" >
            <input type="text" placeholder="Enter your name here!" onChange={(e) => {
                inputName(e.target.value)
            }} />
            {name && <h1>Welcome: {name}</h1>}
            <button onClick={async () => {
                try {
                    const response = await axios.get('/api/ping');
                    inputName(response.data);
                } catch (error) {
                    console.error(error);
                }
            }}>Ping?</button>
        </div >
    );
}

export default Nav;
