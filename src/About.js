import React from 'react';

function About() {
    return (
        <h1>Learning how to use gitlab CI/CD with express to serve my full stack production build. Also includes routing and hooks.</h1>
    );
}

export default About;
